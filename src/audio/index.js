import Gentrification from './ES_Gentrification - Martin Klem.mp3';
import GermanTownSong from './ES_German Town Song - Magnus Ringblom.mp3';
import HamburgMarchingBand from './ES_Hamburg Marching Band - Magnus Ringblom.mp3';
import RhytmicAcousticGuitar12 from './ES_Rhytmic Acoustic Guitar 12 - Anders Ekengren.mp3';
import Satin from './ES_Satin - Örjan Karlsson.mp3';
import Ufological from './ES_Ufological - Martin Klem.mp3';

import dataList from './data';
import {
  EASY,
  NORMAL,
  HARD,
  INSANE,
} from '../constants/difficulty';

const tracklist = [
  {
    id: 'gentrification-easy',
    song: Gentrification,
    name: 'Gentrification',
    artist: 'Martin Klem',
    difficulty: EASY,
    data: dataList.Gentrification1,
    duration: 30000,
  },
  {
    id: 'germantownsong-easy',
    song: GermanTownSong,
    name: 'German Town Song',
    artist: 'Magnus Ringblom',
    difficulty: EASY,
    data: [],
    duration: 30000,
  },
  {
    id: 'hamburgmarchingband-easy',
    song: HamburgMarchingBand,
    name: 'Hamburg Marching Band',
    artist: 'Magnus Ringblom',
    difficulty: EASY,
    data: [],
    duration: 30000,
  },
  {
    id: 'rhytmicacousticguitar-easy',
    song: RhytmicAcousticGuitar12,
    name: 'Rhytmic Acoustic Guitar 12',
    artist: 'Anders Ekengren',
    difficulty: EASY,
    data: [],
    duration: 30000,
  },
  {
    id: 'satin-easy',
    song: Satin,
    name: 'Satin',
    artist: 'Örjan Karlsson',
    difficulty: EASY,
    data: [],
    duration: 30000,
  },
  {
    id: 'ufological-easy',
    song: Ufological,
    name: 'Ufological',
    artist: 'Martin Klem',
    difficulty: EASY,
    data: [],
    duration: 30000,
  },
];

export default tracklist;
