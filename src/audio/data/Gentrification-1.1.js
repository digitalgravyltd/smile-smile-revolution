// Gentrification
import {
  HAPPY,
  SAD,
  SURPRISED,
} from '../../constants/emotions';

const data = [
  {
    time: 1000,
    emotion: HAPPY
  },
  {
    time: 1708,
    emotion: HAPPY
  },
  {
    time: 2416,
    emotion: HAPPY
  },
  {
    time: 3124,
    emotion: HAPPY
  },
  {
    time: 3832,
    emotion: SAD
  },
  {
    time: 4540,
    emotion: SAD
  },
  {
    time: 5248,
    emotion: SAD
  },
  {
    time: 5956,
    emotion: SAD
  },
  {
    time: 6664,
    emotion: HAPPY
  },
  {
    time: 7372,
    emotion: HAPPY
  },
  {
    time: 8080,
    emotion: HAPPY
  },
  {
    time: 8788,
    emotion: HAPPY
  },
  {
    time: 9496,
    emotion: SAD
  },
  {
    time: 10204,
    emotion: SAD
  },
  {
    time: 10912,
    emotion: SAD
  },
  {
    time: 11620,
    emotion: SAD
  },
  {
    time: 12328,
    emotion: HAPPY
  },
  {
    time: 13036,
    emotion: HAPPY
  },
  {
    time: 13744,
    emotion: HAPPY
  },
  {
    time: 14452,
    emotion: HAPPY
  },
  {
    time: 15160,
    emotion: SAD
  },
  {
    time: 15868,
    emotion: SAD
  },
  {
    time: 16576,
    emotion: SAD
  },
  {
    time: 17284,
    emotion: SAD
  },
  {
    time: 17992,
    emotion: HAPPY
  },
  {
    time: 18700,
    emotion: HAPPY
  },
  {
    time: 19408,
    emotion: HAPPY
  },
  {
    time: 20116,
    emotion: HAPPY
  },
  {
    time: 20824,
    emotion: SAD
  },
  {
    time: 21532,
    emotion: SAD
  },
  {
    time: 22240,
    emotion: SAD
  },
  {
    time: 22948,
    emotion: SAD
  },
  {
    time: 23656,
    emotion: HAPPY
  },
  {
    time: 24364,
    emotion: HAPPY
  },
  {
    time: 25072,
    emotion: HAPPY
  },
  {
    time: 25780,
    emotion: HAPPY
  },
  {
    time: 26488,
    emotion: SAD
  }
];

export default data;
