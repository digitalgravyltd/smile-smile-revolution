// Gentrification
import {
  HAPPY,
  ANGRY,
  SURPRISED,
} from '../../constants/emotions';

const data = [
  {
    time: 1000,
    emotion: HAPPY
  },
  {
    time: 1708,
    emotion: HAPPY
  },
  {
    time: 2416,
    emotion: HAPPY
  },
  {
    time: 3124,
    emotion: HAPPY
  },
  {
    time: 3832,
    emotion: ANGRY
  },
  {
    time: 4540,
    emotion: ANGRY
  },
  {
    time: 5248,
    emotion: ANGRY
  },
  {
    time: 5956,
    emotion: ANGRY
  },
  {
    time: 6664,
    emotion: HAPPY
  },
  {
    time: 7372,
    emotion: HAPPY
  },
  {
    time: 8080,
    emotion: HAPPY
  },
  {
    time: 8788,
    emotion: HAPPY
  },
  {
    time: 9496,
    emotion: ANGRY
  },
  {
    time: 10204,
    emotion: ANGRY
  },
  {
    time: 10912,
    emotion: ANGRY
  },
  {
    time: 11620,
    emotion: ANGRY
  },
  {
    time: 12328,
    emotion: HAPPY
  },
  {
    time: 13036,
    emotion: HAPPY
  },
  {
    time: 13744,
    emotion: HAPPY
  },
  {
    time: 14452,
    emotion: HAPPY
  },
  {
    time: 15160,
    emotion: ANGRY
  },
  {
    time: 15868,
    emotion: ANGRY
  },
  {
    time: 16576,
    emotion: ANGRY
  },
  {
    time: 17284,
    emotion: ANGRY
  },
  {
    time: 17992,
    emotion: HAPPY
  },
  {
    time: 18700,
    emotion: HAPPY
  },
  {
    time: 19408,
    emotion: HAPPY
  },
  {
    time: 20116,
    emotion: HAPPY
  },
  {
    time: 20824,
    emotion: ANGRY
  },
  {
    time: 21532,
    emotion: ANGRY
  },
  {
    time: 22240,
    emotion: ANGRY
  },
  {
    time: 22948,
    emotion: ANGRY
  },
  {
    time: 23656,
    emotion: HAPPY
  },
  {
    time: 24364,
    emotion: HAPPY
  },
  {
    time: 25072,
    emotion: HAPPY
  },
  {
    time: 25780,
    emotion: HAPPY
  },
  {
    time: 26488,
    emotion: ANGRY
  }
];

export default data;
