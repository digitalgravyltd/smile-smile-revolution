import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Canvas extends Component {

  constructor(props) {
    super(props);
    this.element = null;
    this.ctx = null;
    this.timer = null;
    this.tick = 0;
    this.startTime = null;
    this.state = {
      layers: props.layers,
    };
  }
  
  shouldComponentUpdate() {
    return false;
  }
  
  componentWillReceiveProps(nextProps) {
    this.setState({ layers: nexProps.layers });
  }
  
  setRef = (ref) => {
    this.element = ref;
    this.ctx = this.element.getContext('2d');
    this.startTime = Date.now();
    this.loop();
  }
  
  loop = () => {
    this.tick = this.tick + 1;
    this.timer = Date.now() - this.timer;
    const { height, width } = this.props;
    this.ctx.clearRect(0, 0, width, height);
    const { layers } = this.state;
    layers.forEach((layer) => layer({
      ctx: this.ctx,
      height,
      width,
      tick: this.tick,
      timer: this.timer,
    }));
    window.requestAnimationFrame(this.loop);
  }
  
  render() {
    const {
      width,
      height,
      className,
    } = this.props;
    const classes = `canvas ${className}`;
    return (
      <canvas
        className={classes}
        width={width}
        height={height}
        ref={(ref) => this.setRef(ref)}
      />
    );
  }

}

Canvas.propTypes = {
  className: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  layers: PropTypes.arrayOf(PropTypes.func),
};

Canvas.defaultProps = {
  className: '',
  height: 300,
  width: 400,
  layers: [],
};

export default Canvas;
