import {
  ANGRY,
  HAPPY,
  SAD,
  SURPRISED,
} from '../constants/emotions';


const emotionMap = (emotion) => {
  let emoji;
  switch (emotion) {
    case ANGRY:
      emoji = '😡';
      break;
    case HAPPY:
      emoji = '😃';
      break;
    case SAD:
      emoji = '😢';
      break;
    case SURPRISED:
      emoji = '😲';
      break;
    default:
      emoji = '😐';
      break;
  }
  return emoji;
};

export default emotionMap;
