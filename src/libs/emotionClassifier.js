class emotionClassifier {
  constructor (modelCollection) {
    this.previousParameters = [];
    this.classifier = {};
    this.emotions = [];
    this.coefficient_length = 0;

    this.emotions = Object.keys(modelCollection).map((modelName) => {
      const model = modelCollection[modelName];
      this.classifier[modelName] = {
        bias: model.bias,
        coefficients: model.coefficients,
      };
      return modelName;
    });
    this.coefficient_length = this.classifier[this.emotions[0]].coefficients.length;
  }

  getEmotions() {
    return this.emotions;
  }

  getBlank () {
    return this.emotions.map(emotion => ({ emotion, value: 0 }));
  }

  predict (parameters) {
    return this.emotions.map((emotion) => {
      const bias = this.classifier[emotion].bias;
      const score = Array.from(Array(this.coefficient_length).keys())
        .reduce((accumulator, currentValue) => {
          return accumulator
            + this.classifier[emotion].coefficients[currentValue]
            * parameters[currentValue + 6];
        }, bias);
      const value = 1 / (1 + Math.exp(-score));
      return {
        emotion,
        value,
      };
    });
  }

  addPreviousParam(parameters) {
    this.previousParameters = [
      ...this.previousParameters,
      parameters,
    ];
    if(this.previousParameters.length > 10) {
      this.previousParameters = this.previousParameters.slice(1);
    }
  }

  meanPredict(parameters) {
    this.addPreviousParam(parameters.slice(0));
    let mean = false;
    if(this.previousParameters.length > 9) {
      mean = this.previousParameters.map((parameter) =>
        parameter.reduce((accumulator, currentValue) => accumulator + currentValue, 0)
      );
      mean = mean.map(val => val /= 10);
    }
    return this.predict(mean);
  }
}

export default emotionClassifier;
