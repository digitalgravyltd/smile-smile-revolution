import {
  ABORT_GAME,
  END_GAME,
  SET_TRACK,
  START_GAME,
  UPDATE_EMOTIONS,
  UPDATE_SCORE,
  UPDATE_SCORE_FOR_INDEX,
} from './actionTypes';

export const endGame = () => ({ type: END_GAME });
export const startGame = () => ({ type: START_GAME });

export const updateScore = score => ({ type: UPDATE_SCORE, payload: score });

export const updateEmotions = (emotions) => {
  let dispatcher = { type: 'null' };
  if (typeof emotions === 'object' && Array.isArray(emotions)) {
    dispatcher = { type: UPDATE_EMOTIONS, payload: emotions };
  }
  return dispatcher;
};

export const setTrack = track => ({ type: SET_TRACK, payload: track });

export const scoreIndex = ({ index, value }) => ({
  type: UPDATE_SCORE_FOR_INDEX,
  payload: { index, value }
});

export const abortGame = () => ({ type: ABORT_GAME });
