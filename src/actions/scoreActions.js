import LocalStorageDB from 'localstoragedb';
import uuid from 'uuid4';

import {
  GET_SCORES,
  RESET_SCORES,
  SUBMITTED_SCORE,
} from './actionTypes';

const DB = new LocalStorageDB('ssr', window.localStorage);
if (DB.isNew()) {
  DB.createTable('scores', ['id', 'trackId', 'name', 'value', 'timestamp']);
  DB.insert('scores', {
    id: uuid(),
    trackId: 'test',
    name: 'test',
    value: 0,
    timestamp: Date.now(),
  });
  DB.commit();
}

export const getScores = trackId => (dispatch) => {
  const results = DB.queryAll('scores', {
    query: {
      trackId,
    },
    limit: 15,
    sort: [
      ['value', 'DESC'],
    ],
  });

  dispatch({
    type: GET_SCORES,
    payload: {
      trackId,
      results,
    },
  });
};

export const setScore = ({ trackId, score, name }) => (dispatch) => {
  DB.insert('scores', {
    id: uuid(),
    trackId,
    name,
    value: score,
    timestamp: Date.now(),
  });
  DB.commit();

  dispatch({
    type: SUBMITTED_SCORE,
  });
  window.location = '/';
};

export const resetScores = () => ({ type: RESET_SCORES });
