import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { Router, Route, Redirect, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import Debug from './containers/Debug';
import Game from './containers/Game';
import Page from './containers/Page';
import Selector from './containers/Selector';
import rootReducer from './reducers';
import enableDevTools from './util/enableDevTools';

import './fonts/fonts.scss';
import './index.scss';

const storeEnhancer = compose(
  applyMiddleware(thunk),
  enableDevTools,
);

const store = createStore(
  rootReducer,
  storeEnhancer,
);

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/game" component={Game} />
      <Route path="/debug" component={Debug} />
      <Route path="/" component={Selector}>
        {/* Sub-routes here */}
        <Redirect from="*" to="/" />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
