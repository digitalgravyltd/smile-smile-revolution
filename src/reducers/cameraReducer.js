import {
  UPDATE_EMOTIONS,
} from '../actions/actionTypes';

const initialState = {
  emotions: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_EMOTIONS:
      return {
        ...state,
        emotions: action.payload,
      };

    default:
      return state;
  }
};
