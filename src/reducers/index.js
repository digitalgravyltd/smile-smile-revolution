import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import gameReducer from './gameReducer';
import cameraReducer from './cameraReducer';

export default combineReducers({
  game: gameReducer,
  camera: cameraReducer,
  routing: routerReducer,
});
