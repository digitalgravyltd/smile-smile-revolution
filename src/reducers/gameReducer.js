import {
  ABORT_GAME,
  END_GAME,
  SET_TRACK,
  START_GAME,
  UPDATE_SCORE,
  UPDATE_SCORE_FOR_INDEX,
  GET_SCORES,
  RESET_SCORES,
} from '../actions/actionTypes';
import {
  PAUSED,
  PLAYING,
  FINISHED,
} from '../constants/status';
import {
  EASY,
  NORMAL,
  HARD,
  INSANE,
} from '../constants/difficulty';

const initialState = {
  status: PAUSED,
  score: 0,
  difficulty: EASY,
  track: null,
  loadingScores: true,
  scoresTrackId: null,
  scores: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET_SCORES:
      return {
        ...state,
        scores: [],
        loadingScores: true,
        scoresTrackId: null,
      };

    case GET_SCORES:
      return {
        ...state,
        loadingScores: false,
        scores: action.payload.results,
        scoresTrackId: action.payload.trackId,
      };

    case START_GAME:
      return {
        ...state,
        status: PLAYING,
      };

    case END_GAME:
      return {
        ...state,
        status: FINISHED,
      };

    case UPDATE_SCORE:
      return {
        ...state,
        score: action.payload,
      };

    case ABORT_GAME:
      return {
        ...state,
        score: 0,
        status: FINISHED,
      };

    case UPDATE_SCORE_FOR_INDEX: {
      const shouldScore = !state.track.data[action.payload.index].triggered;
      // console.log('UPDATE_SCORE_FOR_INDEX', shouldScore, state.score + action.payload.value);
      return {
        ...state,
        score: shouldScore ? state.score + action.payload.value : state.score,
        track: {
          ...state.track,
          data: state.track.data.map((data, i) => ({
            ...data,
            triggered: i === action.payload.index ? true : data.triggered,
          })),
        },
      };
    }

    case SET_TRACK:
      return {
        ...state,
        track: {
          ...action.payload,
          data: action.payload.data.map(data => ({ ...data, triggered: false })),
        },
      };

    default:
      return state;
  }
};
