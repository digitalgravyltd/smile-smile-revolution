import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import pModel from '../../libs/model_pca_20_svm';
import clm from '../../libs/clmtrackr.module';
import EmotionClassifier from '../../libs/emotion_classifier';
import emotionModel from '../../libs/emotionmodel';

import {
  updateEmotions,
} from '../../actions/gameActions';

class Tracker extends Component {
  constructor(props) {
    super(props);
    this.canvasElement = null;
    this.ctx = null;
    this.ctrack = null;
    this.emotionClassifier = null;
    this.trackingStarted = false;
    this.alive = true;
  }

  shouldComponentUpdate() {
    return false;
  }

  componentWillUnmount() {
    this.alive = false;
  }

  setupCanvas = (ref) => {
    this.canvasElement = ref;
    if (this.canvasElement) {
      this.ctx = this.canvasElement.getContext('2d');

      // set eigenvector 9 and 11 to not be regularized.
      // This is to better detect motion of the eyebrows
      pModel.shapeModel.nonRegularizedVectors.push(9);
      pModel.shapeModel.nonRegularizedVectors.push(11);

      this.emotionClassifier = new EmotionClassifier();
      const eModel = {
        ...emotionModel,
      };
      delete eModel.disgusted;
      delete eModel.fear;
      delete eModel.SAD;
      // delete eModel.ANGRY;
      delete eModel.SURPRISED;
      this.emotionClassifier.init(eModel);
      this.emotionData = this.emotionClassifier.getBlank();

      // eslint-disable-next-line
      this.ctrack = new clm.tracker({ useWebGL: true });
      this.ctrack.init(pModel);
      this.ctrack.start(this.props.videoElement);
      this.trackingStarted = true;
      this.drawLoop();
    }
  }

  drawLoop = () => {
    if (this.alive) {
      const { videoElement } = this.props;
      const vidWidth = videoElement.width;
      const vidHeight = videoElement.height;
      this.ctx.clearRect(0, 0, vidWidth, vidHeight);
      if (this.props.showDebug && this.ctrack.getCurrentPosition()) {
        this.ctrack.draw(this.canvasElement);
      }
      const currentParameters = this.ctrack.getCurrentParameters();
      this.props.updateEmotions(this.emotionClassifier.meanPredict(currentParameters));
      setTimeout(() => this.drawLoop(), 150);
    }
  }

  render() {
    return (
      <canvas
        className={this.props.className}
        width="400"
        height="300"
        ref={(ref) => { this.setupCanvas(ref); }}
      />
    );
  }
}

Tracker.propTypes = {
  className: PropTypes.string,
  updateEmotions: PropTypes.func.isRequired,
  videoElement: PropTypes.shape().isRequired,
  showDebug: PropTypes.bool,
};

Tracker.defaultProps = {
  className: '',
  showDebug: false,
};

const mapStateToProps = null;

const mapDispatchToProps = {
  updateEmotions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Tracker);
