import React, { Component } from 'react';

import Tracker from '../Tracker';
import Video from '../Video';

import './page.scss';

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videoElement: null,
    };
  }

  getVideoElement = (videoElement) => {
    this.setState({
      videoElement,
    });
  }

  render() {
    const {
      videoElement,
    } = this.state;
    return (
      <div>
        <div className="container">
          <Video
            className="video"
            getElement={this.getVideoElement}
          />
          {
            videoElement
            ? (
              <Tracker
                className="canvas"
                videoElement={videoElement}
              />
            )
            : null
          }
        </div>
      </div>
    );
  }
}

export default Page;
