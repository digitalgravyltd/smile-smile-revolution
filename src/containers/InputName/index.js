/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './inputName.scss';

const characters = [
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '?', '_',
];

class InputName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      maxLength: 5,
    };
  }

  selectCharacter = (char) => {
    const {
      name,
      maxLength,
    } = this.state;
    if (name.length < maxLength) {
      this.setState({
        name: `${name}${char}`,
      });
    }
  }

  deleteLastChar = () => {
    const {
      name,
    } = this.state;
    const brokenName = name.split('');
    brokenName.pop();
    const newName = brokenName.join('');
    this.setState({
      name: newName,
    });
  }

  render() {
    const {
      name,
      maxLength,
    } = this.state;
    const brokenName = name.split('');
    const inputtedName = new Array(maxLength).fill('').map((c, i) => {
      let retVal = ' ';
      if (brokenName.length >= i) {
        retVal = brokenName[i];
      }
      return retVal;
    });

    return <div className="inputName">
      <h1 className="inputName__heading">Congratulations!</h1>
      <h2 className="inputName__score">You scored: {this.props.score}</h2>
      <p className="inputName__name">Your name:</p>
      <ul className="inputName__input">
        {
          inputtedName.map((char, i) => <li className="inputName__input__item" key={`char-${i}${char}`}>{char}</li>)
        }
      </ul>
      <ul className="inputName__list">
        {
          characters.map(char => <li key={char} className="inputName__list__item">
            <button
              className="inputName__list__item__button"
              onClick={() => this.selectCharacter(char)}
            >
              {char}
            </button>
          </li>)
        }
        <li className="inputName__list__item">
          <button
            className="inputName__list__item__button inputName__list__item__button--delete"
            onClick={() => this.deleteLastChar()}
          >
            Del
          </button>
        </li>
        <li className="inputName__list__item">
          <button
            className="inputName__list__item__button inputName__list__item__button--ok"
            onClick={() => this.props.onComplete(name)}
          >
            OK
          </button>
        </li>
      </ul>
    </div>
  }
}

InputName.propTypes = {
  onComplete: PropTypes.func.isRequired,
  score: PropTypes.number.isRequired,
};

export default InputName;
