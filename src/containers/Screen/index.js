import React from 'react';
import PropTypes from 'prop-types';

import './screen.scss';

const Screen = ({ children }) => (
  <section className="screen">
    <h1 className="titleFace">Smile Smile<br />Revolution</h1>
    { children }
  </section>
);

Screen.propTypes = {
  children: PropTypes.node,
};

Screen.defaultProps = {
  children: null,
};

export default Screen;
