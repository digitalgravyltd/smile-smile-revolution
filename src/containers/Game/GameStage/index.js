/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import {
  Circle,
  Filters,
  Group,
  Layer,
  Line,
  Stage,
  Text,
} from 'react-konva';

import Countdown from '../Countdown';

import {
  startGame,
  scoreIndex,
  endGame,
  abortGame,
} from '../../../actions/gameActions';

import {
  ANGRY,
  HAPPY,
  SAD,
  SURPRISED,
} from '../../../constants/emotions';

import {
  EASY,
  NORMAL,
  HARD,
  INSANE,
} from '../../../constants/difficulty';

import {
  PAUSED,
  PLAYING,
  FINISHED,
} from '../../../constants/status';

import EmotionMap from '../../../util/emotionMap';

import './gameStage.scss';

const LEAD_IN_TIME = 5000;
const LEAD_OUT_TIME = 500;
const TARGET_Y = 150;
const EMOJI_SIZE = 50;
const PADDING = 100;

const scoreMultiplier = {
  [EASY]: 100,
  [NORMAL]: 250,
  [HARD]: 400,
  [INSANE]: 800,
}

const difficultyColumns = {
  [EASY]: [HAPPY, ANGRY],
  [NORMAL]: [HAPPY, SAD, SURPRISED],
  [HARD]: [HAPPY, SAD, ANGRY, SURPRISED],
  [INSANE]: [HAPPY, SAD, ANGRY, SURPRISED],
};

let columnXPosition = {
  [ANGRY]: 0,
  [HAPPY]: 0,
  [SAD]: 0,
  [SURPRISED]: 0,
};

let columnWidth = 0;

const emojiColors = {
  [ANGRY]: '#FE3E53',
  [HAPPY]: '#A5E951',
  [SAD]: '#3797D3',
  [SURPRISED]: '#FCC73C',
};

const renderEmotionTracking = ({
  difficulty,
  emotions,
}) => {
  let needlePos = 0;
  let columnMarkers = null;
  let value = 0;
  // work out where on a scale the needle should be
  if (difficultyColumns[difficulty].length === 2) {
    if (emotions.length > 0) {
      // put the needle near emotions[0]
      let base = 0;
      let end = 0;
      const onePercent = (columnWidth / 2) / 100;
      value = parseInt((emotions[0].value * 100) * 4, 10);
      if (value > 100) {
        value = 100;
      }
      if (emotions[0].emotion === HAPPY) {
        base = columnXPosition[HAPPY] + columnWidth / 2;
        end = columnXPosition[HAPPY];
        needlePos = base - onePercent * value;
      } else {
        base = columnXPosition[ANGRY] - columnWidth / 2;
        end = columnXPosition[ANGRY];
        needlePos = base + onePercent * value;
      }
      // columnMarkers = difficultyColumns[difficulty].map((emotion, i) =>
      //   <Line
      //     key={`columnmarker-${i}`}
      //     points={[PADDING + i * columnWidth, 0, PADDING + i * columnWidth, 100]}
      //     stroke="red"
      //     width={1}
      //   />
      // )
      // columnMarkers = [
      //   <Line
      //     key="columnmarker-base"
      //     points={[base, 0, base, 100]}
      //     stroke="pink"
      //     width={1}
      //   />,
      //   <Line
      //     key="columnmarker-end"
      //     points={[end, 0, end, 100]}
      //     stroke="green"
      //     width={1}
      //   />,
      // ];
    }
  }
  // return (
  //   <Layer>
  //     <Line
  //       points={[
  //         needlePos, 0,
  //         needlePos + 8, 0,
  //         needlePos, TARGET_Y,
  //         needlePos - 8, 0,
  //         needlePos, 0,
  //       ]}
  //       fill="#FB9524"
  //       stroke="#E0861F"
  //       width={3}
  //       closed
  //     />
  //     {/* { columnMarkers } */}
  //   </Layer>
  // )
  return <div
    style={{
      left: needlePos
    }}
    className="gameStage__needle"
  >
    {
      emotions.length > 0
        ? <span
          className="gameStage__needle__emotion"
          style={{
            fontSize: `${(value / 100) * 4}em`
          }}
        >
          {EmotionMap(emotions[0].emotion)}
        </span>
        : null
    }
  </div>
};

const renderTargets = ({
  difficulty,
  height,
  width,
}) => {
  const columns = difficultyColumns[difficulty] || [];
  const playWidth = width - (PADDING * 2);
  columnWidth = playWidth / columns.length;
  columns.forEach((emotion, i) => {
    columnXPosition[emotion] = ((i+1) * columnWidth) - (columnWidth / 2) + PADDING;
  });
  // const hundredSplits = new Array(Math.floor(width / 100)).fill('1');
  return (
    <Layer>
      {/* <Group opacity={0}>
        {
          hundredSplits.map((x,i) => 
            <Line
              key={`hundred-${i}`}
              points={[i*100, 0, i*100, 100]}
              stroke="red"
              width="1"
            />
          )
        }
      </Group>
      <Group>
        <Line
          points={[PADDING, 0, PADDING, 100]}
          stroke="red"
          width={1}
        />
        <Line
          points={[width - PADDING, 0, width - PADDING, 100]}
          stroke="red"
          width={1}
        />
      </Group> */}
      <Group>
        {
          columns.map((emotion) => 
            <Circle
              key={`target-${emotion}`}
              x={columnXPosition[emotion]}
              y={TARGET_Y}
              radius={EMOJI_SIZE * 0.7}
              stroke={emojiColors[emotion]}
              strokeWidth="8"
            />
          )
        }
      </Group>
    </Layer>
  );
};

const renderEmotions = ({
  data,
  difficulty,
  height,
  mainEmotion,
  score,
  scoreIndex,
  timer,
  width,
}) => (
  <Layer>
    {
      data.map(({ time: origTime, emotion, triggered }, index) => {
        const time = origTime + LEAD_IN_TIME;
        const emoji = EmotionMap(emotion);
        if (
          (time >= timer && timer >= origTime)
          || (time + LEAD_OUT_TIME >= timer)
         ) {
          const movePerMs = (height - TARGET_Y) / LEAD_IN_TIME;
          const y = TARGET_Y + (movePerMs * (time - timer));
          const halfHeight = height / 2;
          const pixelPercentageHalfWay = 1 - ((100 / halfHeight) * (y - halfHeight) ) / 100;
          const opacity = y > height ? 0 : pixelPercentageHalfWay;
          const color = emojiColors[emotion];
          const trailSize = EMOJI_SIZE * 0.48;
          const trailX = EMOJI_SIZE / 2;
          const offsetY = 15;
          if (y < TARGET_Y && !triggered) {
            let value = 0;
            if (mainEmotion.emotion === emotion) {
              value = mainEmotion.value * 2;
              if (value > 1) {
                value = 1;
              }
              value = parseInt(value * scoreMultiplier[difficulty], 10);
            }
            setTimeout(() => scoreIndex({index, value}), 0);
          }
          return (
            <Group
              key={`${time}-${emotion}`}
              x={columnXPosition[emotion] - (EMOJI_SIZE / 2)}
              y={y}
              opacity={opacity}
            >
              {/* <Group opacity="1">
                <Circle x={trailX} y={offsetY + 50} radius={trailSize} fill={color} opacity="0.05" />
                <Circle x={trailX} y={offsetY + 45} radius={trailSize} fill={color} opacity="0.05" />
                <Circle x={trailX} y={offsetY + 40} radius={trailSize} fill={color} opacity="0.1" />
                <Circle x={trailX} y={offsetY + 35} radius={trailSize} fill={color} opacity="0.1" />
                <Circle x={trailX} y={offsetY + 30} radius={trailSize} fill={color} opacity="0.1" />
                <Circle x={trailX} y={offsetY + 25} radius={trailSize} fill={color} opacity="0.15" />
                <Circle x={trailX} y={offsetY + 20} radius={trailSize} fill={color} opacity="0.15" />
                <Circle x={trailX} y={offsetY + 15} radius={trailSize} fill={color} opacity="0.15" />
                <Circle x={trailX} y={offsetY + 10} radius={trailSize} fill={color} opacity="0.2" />
                <Circle x={trailX} y={offsetY + 5} radius={trailSize} fill={color}  opacity="0.2" />
              </Group> */}
              <Text
                text={emoji}
                fontSize={EMOJI_SIZE}
                x="0"
                y="0"
                opacity="1"
                align="center"
              />
            </Group>
          );
        } else {
          return null;
        }
      })
    }
  </Layer>
);

class GameStage extends Component {
  constructor(props) {
    super(props);
    if (!props.track || !props.track.id) {
      window.location = '/';
    }
    this.startTime = null;
    if (props.track && props.track.duration) {
      this.duration = props.track.duration + LEAD_IN_TIME + LEAD_OUT_TIME;
    }
    this.state = {
      timer: null,
    };
  }

  componentDidMount() {
    this.start();
  }

  start = () => {
    this.startTime = Date.now();
    this.props.startGame();
    this.setState({
      timer: 0,
    });
    this.loop();
  }

  stop = () => {
    this.startTime = Date.now();
    this.props.endGame();
    this.setState({
      timer: null,
    });
  }

  abort = () => {
    this.startTime = Date.now();
    this.props.abortGame();
    this.setState({
      timer: null,
    });
  }

  loop = () => {
    const timer = Date.now() - this.startTime;
    this.setState({
      timer,
    }, () => {
      if (timer <= this.duration && this.props.status === PLAYING) {
        window.requestAnimationFrame(this.loop);
      } else if(this.props.status === PLAYING) {
        this.props.finishGame({
          score: this.props.score,
          trackId: this.props.track.id,
        });
      } else {
        this.setState({
          timer: null,
        });
      }
    });
  }

  render() {
    const {
      timer,
    } = this.state;
    const {
      difficulty,
      emotions,
      height,
      status,
      score,
      scoreIndex,
      track,
      width,
    } = this.props;

    const {
      data,
      difficulty: trackDifficulty,
    } = track || { data: null, difficulty: null };

    const orderedEmotions = emotions.sort((a,b) => {
      if (a.value < b.value) {
        return 1;
      } else if (a.value > b.value) {
        return -1;
      } else {
        return 0;
      }
    });

    const trimmedEmotions = [];
    orderedEmotions.forEach(({ emotion, value }) => {
      if (difficultyColumns[difficulty].indexOf(emotion) > -1) {
        trimmedEmotions.push({emotion, value});
      }
    });

    const mainEmotion = trimmedEmotions.length > 0
      ? trimmedEmotions[0]
      : { emotion: 'unknown', value: 0 };

    return data ? (
      <section className="gameStage">
        <p className="gameStage__score">Score: {score}</p>
        <p className="gameStage__timer">Timer: {timer}</p>
        <p className="gameStage__difficulty">Difficulty: {difficulty}</p>
        <p className="gameStage__emotion">
          Emotion: {EmotionMap(mainEmotion.emotion)} {parseInt(mainEmotion.value * 100, 10)}%
        </p>
        {/* <div className="gameStage__buttons">
          <button
            className="gameStage__buttons__start"
            onClick={() => this.start()}
            style={{
              opacity: status === PLAYING ? 0.4 : 1
            }}
          >Start</button>
          <button
            className="gameStage__buttons__abort"
            onClick={() => this.abort()}
            style={{
              opacity: status === PLAYING ? 1 : 0.4
            }}
          >Abort</button>
        </div> */}
        <Countdown />
        <Stage width={width} height={height}>
          {
            status === PLAYING
              ? renderEmotions({
                data,
                difficulty,
                height,
                mainEmotion,
                score,
                scoreIndex,
                timer,
                width,
              })
              : null
          }
          {
            renderTargets({
              difficulty,
              height,
              width,
            })
          }
        </Stage>
        {
          renderEmotionTracking({
            difficulty,
            emotions: trimmedEmotions,
          })
        }
        <Countdown leadTime={LEAD_IN_TIME} />
      </section>
    ) : <div className="gameStage__buttons">
      <button
        className="gameStage__buttons__start"
        onClick={() => window.location = '/'}
      >New Game</button>
    </div>
  }
}

GameStage.propTypes = {
  difficulty: PropTypes.string.isRequired,
  emotions: PropTypes.arrayOf(PropTypes.shape({
    emotion: PropTypes.string,
    value: PropTypes.number,
  })),
  abortGame: PropTypes.func.isRequired,
  endGame: PropTypes.func.isRequired,
  height: PropTypes.number,
  status: PropTypes.string.isRequired,
  score: PropTypes.number.isRequired,
  startGame: PropTypes.func.isRequired,
  track: PropTypes.shape({
    id: PropTypes.string,
    artist: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({
      time: PropTypes.number,
      emotion: PropTypes.string,
    })),
    difficulty: PropTypes.string,
    duration: PropTypes.number,
    name: PropTypes.string,
    song: PropTypes.any,
  }),
  scoreIndex: PropTypes.func.isRequired,
  width: PropTypes.number,
};

GameStage.defaultProps = {
  emotions: [],
  height: 600,
  track: {},
  width: 400,
};

const mapStateToProps = ({
  camera,
  game,
}) => {
  return {
    difficulty: game.difficulty,
    emotions: camera.emotions,
    status: game.status,
    score: game.score,
    track: game.track,
  };
};

const mapDispatchToProps = {
  endGame,
  startGame,
  scoreIndex,
  abortGame,
};

export default connect(mapStateToProps, mapDispatchToProps)(GameStage);
