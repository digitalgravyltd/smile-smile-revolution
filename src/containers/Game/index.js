/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Sound from 'react-sound';
import { connect } from 'react-redux';

import {
  setScore,
} from '../../actions/scoreActions';

import tracklist from '../../audio';

import Page from '../Page';
import GameStage from './GameStage';
import InputName from '../InputName';
import './game.scss';

const LEAD_IN_TIME = 5000;

const SoundStatus = Sound.status;

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      playStatus: SoundStatus.PAUSED,
      showInputName: false,
      score: 0,
      trackId: null,
    };
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
    setTimeout(() => {
      this.setState({
        playStatus: SoundStatus.PLAYING,
      });
    }, LEAD_IN_TIME);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions = () => {
    this.setState({
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    });
  }

  finishGame = ({ score, trackId }) => {
    // alert(`finished game of ${trackId} with a score of ${score}`);
    this.setState({
      showInputName: true,
      score,
      trackId,
    });
  }

  submitNameAndScore = (name) => {
    const {
      score,
      trackId,
    } = this.state;
    // alert(`name: ${name}, score: ${score}, trackid: ${trackId}`);
    this.props.setScore({
      score,
      trackId,
      name,
    });
  }
  
  render() {
    const {
      windowWidth,
      windowHeight,
      playStatus,
      showInputName,
      score,
    } = this.state;
    return (
      <div>
        {
          showInputName ? <InputName onComplete={(name) => this.submitNameAndScore(name)} score={score} />
          : <div>
            <div className="cameraView"><Page /></div>
            <GameStage
              width={windowWidth}
              height={windowHeight}
              finishGame={({ score, trackId }) => this.finishGame({ score, trackId })}
            />
            <Sound url={tracklist[0].song} playStatus={playStatus} />
          </div>
        }
      </div>
    );
  }
}

Game.propTypes = {
  setScore: PropTypes.func.isRequired,
};

Game.defaultProps = {};

const mapDispatchToProps = {
  setScore,
};

export default connect(null, mapDispatchToProps)(Game);
