/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './countdown.scss';

class Countdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: 4,
      showTimer: false,
      timeBetweenCounts: props.leadTime / 4
    };
  }

  componentWillMount() {
    this.countdown();
  }

  countdown = () => {
    const {
      timer,
    } = this.state;
    let newTimer;
    if (timer === 'Go!') {
      newTimer = '';
    } else {
      newTimer = timer - 1;
    }
    if (newTimer === 0){
      newTimer = 'Go!';
    }
    if (newTimer !== '') {
      window.setTimeout(() => this.countdown(), this.state.timeBetweenCounts);
    }
    this.setState({
      timer: newTimer,
      showTimer: true,
    })
  }

  render() {
    const {
      timer,
      showTimer,
    } = this.state;

    return showTimer ? <div className="countdown">{timer}</div> : null;
  }
}

Countdown.propTypes = {
  leadTime: PropTypes.number,
};

Countdown.defaultProps = {
  leadTime: 5000,
}

export default Countdown;
