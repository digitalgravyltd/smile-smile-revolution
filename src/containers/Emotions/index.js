/* eslint-disable */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Emotions extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);
  }

  render() {
    const {
      emotions,
    } = this.props;

    const orderedEmotions = emotions.sort((a,b) => {
      if (a.value < b.value) {
        return 1;
      } else if (a.value > b.value) {
        return -1;
      } else {
        return 0;
      }
    });
    

    return (
      <div className="emotionsList">
        {
          orderedEmotions
          ? orderedEmotions.map(({
            emotion,
            value,
          }) => <p key={emotion}>{emotion}: {value.toFixed(2)}</p>)
          : null
        }
      </div>
    );
  }
}

Emotions.propTypes = {
  emotions: PropTypes.arrayOf(PropTypes.shape({
    emotion: PropTypes.string,
    value: PropTypes.number,
  })),
};

Emotions.defaultProps = {
  emotions: [],
};

export default Emotions;
