import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Tracker from '../Tracker';
import Video from '../Video';
import Emotions from '../Emotions';

import './page.scss';

class Debug extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videoElement: null,
    };
  }

  getVideoElement = (videoElement) => {
    this.setState({
      videoElement,
    });
  }

  render() {
    const {
      videoElement,
    } = this.state;
    const {
      emotions,
    } = this.props;
    return (
      <div>
        <div className="container">
          <Video
            className="video"
            getElement={this.getVideoElement}
          />
          {
            videoElement
            ? (
              <div>
                <Tracker
                  className="canvas"
                  videoElement={videoElement}
                  showDebug
                />
                <div className="emotionsHolder">
                  <Emotions emotions={emotions} />
                </div>
              </div>
            )
            : null
          }
        </div>
      </div>
    );
  }
}

Debug.propTypes = {
  emotions: PropTypes.arrayOf(PropTypes.shape({
    emotion: PropTypes.string,
    value: PropTypes.number,
  })),
};

Debug.defaultProps = {
  emotions: [],
};

const mapStateToProps = ({
  camera,
}) => ({
  emotions: camera.emotions,
});

export default connect(mapStateToProps, null)(Debug);
