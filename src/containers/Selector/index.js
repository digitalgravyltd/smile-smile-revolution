import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Screen from '../Screen';
import Scores from '../Scores';

import { setTrack } from '../../actions/gameActions';
import tracklist from '../../audio';

import './selector.scss';

class Selector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trackId: tracklist[0].id,
    };
  }

  setTrack = (trackId) => {
    if (trackId === 'gentrification-easy') {
      this.setState({
        trackId,
      });
    }
  }

  selectTrack = () => {
    const {
      trackId,
    } = this.state;
    this.props.setTrack(tracklist.find(track => track.id === trackId));
    this.props.router.push('/game');
  }

  render() {
    const {
      trackId,
    } = this.state;
    return (
      <Screen>
        <div className="screen__center tracklist__holder">
          {
            tracklist.map(trackItem =>
              <button
                key={`track-${trackItem.id}`}
                value={trackItem.id}
                className={
                  `tracklist tracklist--${trackItem.difficulty.toLowerCase()} ${
                    trackItem.id === trackId ? 'tracklist--selected' : ''
                  }${
                    trackItem.id !== 'gentrification-easy' ? 'tracklist--disabled' : ''
                  }`
                }
                onClick={() => this.setTrack(trackItem.id)}
              >
                <span className="tracklist__difficulty">{trackItem.difficulty}</span>
                <span className="tracklist__name">{trackItem.name}</span>
                <span className="tracklist__artist">{trackItem.artist}</span>
              </button>
            )
          }
          <button className="tracklist__select" onClick={() => this.selectTrack()}>Select track</button>
          <Scores trackId={trackId} />
        </div>
      </Screen>
    );
  }
}

Selector.propTypes = {
  router: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  setTrack: PropTypes.func.isRequired,
};
Selector.defaultProps = {};

const mapStateToProps = null;

const mapDispatchToProps = {
  setTrack,
};

export default connect(mapStateToProps, mapDispatchToProps)(Selector);
