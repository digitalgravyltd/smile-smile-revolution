import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Video extends Component {
  constructor(props) {
    super(props);
    this.videoElement = null;
    this.hasEvents = false;
  }

  componentDidMount() {
    navigator.mediaDevices.getUserMedia({ video: true })
      .then(stream => this.initialiseWebcam(stream))
      .catch(e => console.log(e));
  }

  shouldComponentUpdate() {
    return false;
  }

  componentWillUnmount() {
    this.videoElement.pause();
    this.videoElement.src = '';
    this.videoElement.srcObject.getVideoTracks().forEach((track) => {
      track.stop();
    });
    this.videoElement.srcObject = undefined;
    this.videoElement.load();
  }

  setupEvents = () => {
    if (!this.hasEvents) {
      const {
        getElement,
      } = this.props;
      this.hasEvents = true;
      this.videoElement.onloadedmetadata = () => {
        try {
          this.videoElement.play();
          getElement(this.videoElement);
        } catch (e) {
          // into the vooooiiiiddddd
        }
      };
    }
  }

  initialiseWebcam = (stream) => {
    this.videoElement.srcObject = stream;
    this.setupEvents();
  }

  render() {
    return (
      <video
        className={this.props.className}
        width="400"
        height="300"
        preload="auto"
        loop
        playsInline
        autoPlay
        ref={(ref) => { this.videoElement = ref; }}
      />
    );
  }
}

Video.propTypes = {
  getElement: PropTypes.func,
  className: PropTypes.string,
};

Video.defaultProps = {
  getElement: () => {},
  className: '',
};

export default Video;
