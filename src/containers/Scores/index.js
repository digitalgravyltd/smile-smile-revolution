import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';

import {
  getScores,
  resetScores,
} from '../../actions/scoreActions';

import './scores.scss';

class Scores extends Component {
  componentWillMount() {
    this.props.getScores(this.props.trackId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.trackId !== this.props.trackId) {
      this.props.resetScores();
      this.props.getScores(nextProps.trackId);
    }
  }

  render() {
    const {
      loading,
      scores,
      trackId,
    } = this.props;

    // console.log('scores: ', scores);

    return (
      <section className="scores">
        <h2 className="scores__heading">Scores</h2>
        {
          loading
            ? <div>Loading...</div>
            : (
              <div>
                <ul
                  className="scores__list"
                >
                  <li className="scores__list__item scores__list__item--header">
                    <span className="scores__list__item__position">Pos</span>
                    <span className="scores__list__item__score">Score</span>
                    <span className="scores__list__item__name">Name</span>
                    <span className="scores__list__item__time">Time</span>
                  </li>
                  {
                    scores && scores.length > 0
                    ? scores.map((score, i) => (
                      <li
                        key={`score-${score.id}`}
                        className={`scores__list__item
                          ${i === 0 ? 'scores__list__item--first' : ''}
                          ${i === 1 ? 'scores__list__item--second' : ''}
                          ${i === 2 ? 'scores__list__item--third' : ''}
                        `}
                      >
                        <span className="scores__list__item__position">{i + 1}.</span>
                        <span className="scores__list__item__score">{score.value}</span>
                        <span className="scores__list__item__name">{score.name}</span>
                        <span className="scores__list__item__time">{moment(score.timestamp).format('HH:mm Do MMM \'YY')}</span>
                      </li>
                      ))
                    : <li className="scores__list__item scores__list__item--none">No scores yet!</li>
                  }
                </ul>
              </div>
            )
        }
      </section>
    );
  }
}

Scores.propTypes = {
  loading: PropTypes.bool.isRequired,
  scores: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    value: PropTypes.number,
    name: PropTypes.string,
    timestamp: PropTypes.timestamp,
  })),
  getScores: PropTypes.func.isRequired,
  resetScores: PropTypes.func.isRequired,
  trackId: PropTypes.string.isRequired,
};

Scores.defaultProps = {
  scores: null,
};

const mapStateToProps = ({
  game,
}) => ({
  scores: game.scores,
  loading: game.loadingScores,
});

const mapDispatchToProps = {
  getScores,
  resetScores,
};

export default connect(mapStateToProps, mapDispatchToProps)(Scores);
